**UPDATE mix-nodes**

Script for updating nym mixnodes. 
This script is intended to ensure and optimise nym mixnode updates.

**USE**

nymnode-update.sh -f https://github.com/nymtech/nym/releases/download/nym-binaries-v2024.1-marabou/nym-mixnodee -n  MYNODEID -p /opt/nym

-f : url to nym-mixnode binary file

-n : nym-mixnode id

-p : place where the executable is stored
