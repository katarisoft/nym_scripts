#! /bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
file="https://github.com/nymtech/nym/releases/download/nym-binaries-v2024.2-fast-and-furious-v2/nym-gateway"
while getopts n:f:p: flag
do
    case "${flag}" in
	n) node=${OPTARG};;
	p) path=${OPTARG};;
    esac
done
if [[ -z ${node} ||  -z ${path} ]]
then
    printf "ERROR: -n idnode and -p path and -f url_bynary_nym-mynode are mandatory arguments. See usage: \n";
    printf "nymgateway-update.sh -n MYNODEI -p /opt/nym \n";
    printf "n : nym-gateway id \n";
    printf "p place where the executable is stored \n";
    exit 1;
fi
mkdir -p ${path}
cd ${path}
printf "${GREEN}Descargando nym-gateway..."
wget -qO nym-gateway.new ${file}
printf "${RED} OK \n ${NC}"
if test -f ${path}/nym-gateway; then
    cp ${path}/nym-gateway ${path}/nym-gateway.old
fi
mv ${path}/nym-gateway.new ${path}/nym-gateway
printf "${GREEN}Cambiando permisos ejecutable..."
chmod +x ${path}/nym-gateway
printf "${RED} OK \n ${NC}"
printf "${GREEN}Iniciando nym-myxnode... \n ${NC}"
${path}/nym-gateway init -o json --id ${node} --host $(curl -4 https://ifconfig.me)
printf "${GREEN}Iniciando nym-myxnode...  ${RED} OK \n ${NC}"
if ! test -f /etc/systemd/system/nym.service; then
    printf "${GREEN}Generando servicio... ${NC} \n"
    printf "[Unit]\nDescription=Nym gateway \nStartLimitInterval=350\nStartLimitBurst=10\n[Service]\nUser=root\nLimitNOFILE=65536\nExecStart=${path}/nym-gateway run --id ${node}\nKillSignal=SIGINT\nRestart=on-failure\nRestartSec=30\n[Install]\nWantedBy=multi-user.target" > nym.service
    sudo mv nym.service /etc/systemd/system/nym.service
    sudo systemctl enable nym.service
    printf "${GREEN}Generando servicio... ${RED} OK \n ${NC}"
fi
printf "${GREEN}Corriendo demonio... ${NC}"
#sudo systemctl daemon-reload
sudo systemctl restart nym.service
printf "${RED} OK \n ${NC}"
