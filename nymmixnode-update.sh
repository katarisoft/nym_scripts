#! /bin/bash
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'
while getopts n:f:p: flag
do
    case "${flag}" in
        f) file=${OPTARG};;
	n) node=${OPTARG};;
	p) path=${OPTARG};;
    esac
done
if [[ -z ${file}  ||  -z ${node} ||  -z ${path} ]]
then
    printf "ERROR: -n idnode and -p path and -f url_bynary_nym-mynode are mandatory arguments. See usage: \n";
    printf "nymnode-update.sh -f https://github.com/nymtech/nym/releases/download/nym-binaries-v2024.1-marabou/nym-mixnodee -n MYNODEI -p /opt/nym \n";
    printf "f : url to nym-mixnode binary file \n";
    printf "n : nym-mixnode id \n";
    printf "p place where the executable is stored \n";
    exit 1;
fi
mkdir -p ${path}
cd ${path}
printf "${GREEN}Descargando nym-mixnode..."
wget -qO nym-mixnode.new ${file}
printf "${RED} OK \n ${NC}"
if test -f ${path}/nym-mixnode; then
    cp ${path}/nym-mixnode ${path}/nym-mixnode.old
fi
mv ${path}/nym-mixnode.new ${path}/nym-mixnode
printf "${GREEN}Cambiando permisos ejecutable..."
chmod +x ${path}/nym-mixnode
printf "${RED} OK \n ${NC}"
printf "${GREEN}Iniciando nym-myxnode... \n ${NC}"
./nym-mixnode init -o json --id ${node} --host $(curl -4 https://ifconfig.me)
printf "${GREEN}Iniciando nym-myxnode...  ${RED} OK \n ${NC}"
if ! test -f /etc/systemd/system/nym.service; then
    printf "${GREEN}Generando servicio... ${NC} \n"
    printf "[Unit]\nDescription=Nym Mixnode \nStartLimitInterval=350\nStartLimitBurst=10\n[Service]\nUser=root\nLimitNOFILE=65536\nExecStart=${path}/nym-mixnode run --id ${node}\nKillSignal=SIGINT\nRestart=on-failure\nRestartSec=30\n[Install]\nWantedBy=multi-user.target" > nym.service
    sudo mv nym.service /etc/systemd/system/nym.service
    sudo systemctl enable nym.service
    printf "${GREEN}Generando servicio... ${RED} OK \n ${NC}"
fi
printf "${GREEN}Corriendo demonio... ${NC}"
#sudo systemctl daemon-reload
sudo systemctl restart nym.service
printf "${RED} OK \n ${NC}"
